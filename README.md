# nani-taberu

<p align="center">
  <a href="https://flutter.io/">
    <img src="https://diegolaballos.com/files/images/flutter-icon.jpg" alt="Logo" width=72 height=72>
  </a>

  <h3 align="center">Nani taberu app</h3>

  <p align="center">
    Example app with Flutter that uses Firebase
    <br>
    Base project made with much  :heart: . Contains CRUD, patterns, and much more!
    <br>
    <br>
    <a href="https://gitlab.com/gaijin1/nani-taberu/issues/new">Report bug</a>
    ·
    <a href="https://gitlab.com/gaijin1/nani-taberu/issues/new">Request feature</a>
  </p>
</p>

## Table of contents

- [Quick start](#quick-start)
- [What's included](#whats-included)
- [Food APIs](#food-apis)

## Quick start

(to edit)
This is a normal flutter <img src="https://diegolaballos.com/files/images/flutter-icon.jpg" alt="Logo" width=36 height=36> app. You should follow the instructions in the [official documentation](https://flutter.io/docs/get-started/install).

## What's included

* Pick up a random meal (TO-DO)
* Search bar, to look for a meal by choosing some criterias (TO-DO)
* Internationalization (TO-DO)
* [Sentry](https://sentry.io)! (logs any error in the app) (TO-DO)

### Food APIs

This flutter app uses couple of different (free) APIs:
* [TheMealDB](https://www.themealdb.com/api.php)
* [Edamam](https://developer.edamam.com/edamam-recipe-api-demo)
* [Spoonacular](https://spoonacular.com/food-api/docs#Get-Recipe-Information)
