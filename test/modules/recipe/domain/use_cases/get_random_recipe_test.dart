import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:nani_taberu/modules/recipe/domain/entities/recipe.dart';
import 'package:nani_taberu/modules/recipe/domain/repositories/recipe_repository.dart';
import 'package:nani_taberu/modules/recipe/domain/use_cases/base_use_case.dart';
import 'package:nani_taberu/modules/recipe/domain/use_cases/get_random_recipe.dart';

class MockRecipeRepository extends Mock implements RecipeRepository {}

void main() {
  GetRandomRecipe usecase;
  MockRecipeRepository mockRecipeRepository;

  setUp(() {
    mockRecipeRepository = MockRecipeRepository();
    usecase = GetRandomRecipe(mockRecipeRepository);
  });

  final tRecipe = Recipe(id: 1);
  test(
    'gets a random recipe from the repository',
    () async {
      when(mockRecipeRepository.getRandomRecipe())
          .thenAnswer((_) async => Right(tRecipe));
      // The "act" phase of the test. Call the not-yet-existent method.
      final result = await usecase(NoParams());
      // UseCase should simply return whatever was returned from the Repository
      expect(result, Right(tRecipe));
      // Verify that the method has been called on the Repository
      verify(mockRecipeRepository.getRandomRecipe());
      // Only the above method should be called and nothing more.
      verifyNoMoreInteractions(mockRecipeRepository);
    },
  );
}
