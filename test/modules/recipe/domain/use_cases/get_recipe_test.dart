import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:nani_taberu/modules/recipe/domain/entities/recipe.dart';
import 'package:nani_taberu/modules/recipe/domain/repositories/recipe_repository.dart';
import 'package:nani_taberu/modules/recipe/domain/use_cases/get_recipe.dart';

class MockRecipeRepository extends Mock implements RecipeRepository {}

void main() {
  GetRecipe usecase;
  MockRecipeRepository mockRecipeRepository;

  setUp(() {
    mockRecipeRepository = MockRecipeRepository();
    usecase = GetRecipe(mockRecipeRepository);
  });

  int tId = 1;
  final tRecipe = Recipe(id: tId);
  test(
    'gets a  recipe from the repository',
    () async {
      when(mockRecipeRepository.getRecipe(any))
          .thenAnswer((_) async => Right(tRecipe));
      final result = await usecase(Params(id: tId));
      expect(result, Right(tRecipe));
      verify(mockRecipeRepository.getRecipe(tId));
      verifyNoMoreInteractions(mockRecipeRepository);
    },
  );
}
