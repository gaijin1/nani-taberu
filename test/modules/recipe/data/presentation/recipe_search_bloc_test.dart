import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:nani_taberu/core/errors/failures.dart';
import 'package:nani_taberu/modules/recipe/domain/entities/recipe.dart';
import 'package:nani_taberu/modules/recipe/domain/use_cases/base_use_case.dart';
import 'package:nani_taberu/modules/recipe/domain/use_cases/get_random_recipe.dart';
import 'package:nani_taberu/modules/recipe/domain/use_cases/get_recipe.dart';
import 'package:nani_taberu/modules/recipe/presentation/bloc/bloc.dart';

class MockGetRandomRecipe extends Mock implements GetRandomRecipe {}

class MockGetRecipe extends Mock implements GetRecipe {}

void main() {
  MockGetRandomRecipe mockGetRandomRecipe;
  MockGetRecipe mockGetRecipe;
  RecipeSearchBloc bloc;

  setUp(() {
    mockGetRandomRecipe = MockGetRandomRecipe();
    mockGetRecipe = MockGetRecipe();
    bloc = RecipeSearchBloc(
      random: mockGetRandomRecipe,
      concrete: mockGetRecipe,
    );
  });

  test('initialState should be InitialRecipeSearchState', () {
    // assert
    expect(bloc.initialState, equals(InitialRecipeSearchState()));
  });

  group('RecipeSearchBloc', () {
    int tId = 222227;
    Recipe tRecipe = Recipe(id: tId, title: 'Test title');

    group('GetRandomRecipe use case', () {
      blocTest(
        'emits [InitialRecipeSearchState, LoadingState, LoadedState] when successful',
        build: () {
          when(mockGetRandomRecipe(any))
              .thenAnswer((_) async => Right(tRecipe));
          return bloc;
        },
        act: (bloc) => bloc.add(GetRandomRecipeEvent()),
        expect: [
          InitialRecipeSearchState(),
          LoadingState(),
          LoadedState(recipe: tRecipe),
        ],
      );

      blocTest(
        'emits [InitialRecipeSearchState, LoadingState, ErrorState] when fetch fails',
        build: () {
          when(mockGetRandomRecipe(any))
              .thenAnswer((_) async => Left(ServerFailure()));
          return bloc;
        },
        act: (bloc) => bloc.add(GetRandomRecipeEvent()),
        expect: [
          InitialRecipeSearchState(),
          LoadingState(),
          ErrorState(message: SERVER_FAILURE_MESSAGE),
        ],
      );

      blocTest(
        'emits [InitialRecipeSearchState, LoadingState, InitialRecipeSearchState] when fetch fails after 3 seconds',
        build: () {
          when(mockGetRandomRecipe(any))
              .thenAnswer((_) async => Left(ServerFailure()));
          return bloc;
        },
        act: (bloc) => bloc.add(InitializeEvent()),
        expect: [
          InitialRecipeSearchState(),
          LoadingState(),
          InitialRecipeSearchState(),
        ],
      );

      blocTest(
        'emits [InitialRecipeSearchState, LoadingState, ErrorState] when laoding data from cache fails',
        build: () {
          when(mockGetRandomRecipe(any))
              .thenAnswer((_) async => Left(CacheFailure()));
          return bloc;
        },
        act: (bloc) => bloc.add(GetRandomRecipeEvent()),
        expect: [
          InitialRecipeSearchState(),
          LoadingState(),
          ErrorState(message: CACHE_FAILURE_MESSAGE),
        ],
      );
    });
  });
}
