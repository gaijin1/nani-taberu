import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:nani_taberu/core/errors/exceptions.dart';
import 'package:nani_taberu/modules/recipe/data/data_sourcs/recipe_local_data_source_impl.dart';
import 'package:nani_taberu/modules/recipe/data/models/recipe_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../fixtures/fixture_reader.dart';

class MockSharedPreferences extends Mock implements SharedPreferences {}

void main() {
  MockSharedPreferences sharedPreferences;
  RecipeLocalDataSourceImpl localDataSource;

  setUp(() {
    sharedPreferences = MockSharedPreferences();
    localDataSource = RecipeLocalDataSourceImpl(preferences: sharedPreferences);
  });

  group('getLastRecipe', () {
    test('returns Recipe from SharedPreferences when there is one in the cache',
        () async {
      // arrange
      int tId = 222227;
      RecipeModel tRecipe = RecipeModel(id: tId);
      when(sharedPreferences.getString(CACHED_RECIPE_KEY))
          .thenAnswer((_) => fixture('spoonacular_recipe.json'));
      // act
      final result = await localDataSource.getLastRecipe();
      // assert
      verify(sharedPreferences.getString(CACHED_RECIPE_KEY));
      expect(result, equals(tRecipe));
    });

    test('returns Recipe from SharedPreferences when there is one in the cache',
        () async {
      // arrange
      when(sharedPreferences.getString(CACHED_RECIPE_KEY)).thenReturn(null);
      // act
      final call = localDataSource.getLastRecipe;
      // assert
      expect(() => call(), throwsA(isA<NoLocalDataException>()));
    });
  });

  group('cacheRecipe', () {
    test('calls SharedPreferences to cache the data', () {
      // arrange
      int tId = 1;
      RecipeModel tRecipe = RecipeModel(id: tId);
      // act
      localDataSource.cacheRecipe(tRecipe);
      // assert
      verify(sharedPreferences.setString(
          CACHED_RECIPE_KEY, json.encode(tRecipe.toJson())));
    });
  });
}
