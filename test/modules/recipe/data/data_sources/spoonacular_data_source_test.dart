import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:nani_taberu/modules/recipe/data/data_sourcs/spoonacular_data_source.dart';
import 'package:nani_taberu/modules/recipe/data/models/recipe_model.dart';
import 'package:nani_taberu/core/errors/exceptions.dart';

import '../../../../fixtures/fixture_reader.dart';

class MockHttpClient extends Mock implements http.Client {}

void main() {
  MockHttpClient mockClient;
  SpoonacularDataSource spoonacularDataSource;
  Map<String, String> headers = {
    'Content-Header': 'application/json',
    'apiKey': 'random_api_key',
  };

  int tId = 222227;
  RecipeModel tRecipeModel = RecipeModel(id: tId);

  setUp(() {
    mockClient = MockHttpClient();
    spoonacularDataSource = SpoonacularDataSource(
      client: mockClient,
      apiKey: 'random_api_key',
    );
  });

  void setSuccessfulRandomRequest() {
    when(mockClient.get(SPOONACULAR_API_RANDOM_RECIPE_URL, headers: headers))
        .thenAnswer((_) async =>
            http.Response(fixture('spoonacular_recipe.json'), 200));
  }

  void setSuccessfulNormalRequest(int number) {
    when(mockClient.get(SPOONACULAR_API_URL + '/$number/information',
            headers: headers))
        .thenAnswer((_) async =>
            http.Response(fixture('spoonacular_recipe.json'), 200));
  }

  void setFailingRandomRequest404() {
    when(mockClient.get(SPOONACULAR_API_RANDOM_RECIPE_URL, headers: headers))
        .thenAnswer(
            (_) async => http.Response('No access to this ressource', 404));
  }

  void setFailingNormalRequest404(int number) {
    when(mockClient.get(SPOONACULAR_API_URL + '/$number/information',
            headers: headers))
        .thenAnswer(
            (_) async => http.Response('No access to this ressource', 404));
  }

  group('getRandomRecipe', () {
    test(
        'makes a GET request on the *recipes/random* endpoints with applicat/json header',
        () async {
      // arrange
      setSuccessfulRandomRequest();
      // act
      await spoonacularDataSource.getRandomRecipe();
      // assert
      verify(
          mockClient.get(SPOONACULAR_API_RANDOM_RECIPE_URL, headers: headers));
    });

    test('returns a Recipe when status code is 200 (success)', () async {
      setSuccessfulRandomRequest();
      // act
      final result = await spoonacularDataSource.getRandomRecipe();
      // assert
      expect(result, equals(tRecipeModel));
    });

    test('throws a ServerException when the response code is 404 or other',
        () async {
      // arrange
      setFailingRandomRequest404();
      // act
      final call = spoonacularDataSource.getRandomRecipe;
      // assert
      expect(() => call(), throwsA(isA<ServerException>()));
    });
  });

  group('getRecipe', () {
    test(
        'makes a GET request on the *recipes/{id}/information* endpoints with applicat/json header',
        () async {
      // arrange
      setSuccessfulNormalRequest(tId);
      // act
      await spoonacularDataSource.getRecipe(tId);
      // assert
      verify(mockClient.get(SPOONACULAR_API_URL + '/$tId/information',
          headers: headers));
    });
    test('returns a Recipe when status code is 200 (success)', () async {
      // arrange
      setSuccessfulNormalRequest(tId);
      // act
      final result = await spoonacularDataSource.getRecipe(tId);
      // assert
      expect(result, equals(tRecipeModel));
    });
    test('throws a ServerException when the response code is 404 or other', () {
      // arrange
      setFailingNormalRequest404(tId);
      // act
      final call = spoonacularDataSource.getRecipe;
      // assert
      expect(() => call(tId), throwsA(isA<ServerException>()));
    });
  });
}
