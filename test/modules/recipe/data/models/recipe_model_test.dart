import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:nani_taberu/modules/recipe/data/models/recipe_model.dart';
import 'package:nani_taberu/modules/recipe/domain/entities/recipe.dart';

import '../../../../fixtures/fixture_reader.dart';

void main() {
  final tId = 222227;
  final tRecipeModel = RecipeModel(id: tId);

  test(
    'should be a subclass of Recipe entity',
    () async {
      // assert
      expect(tRecipeModel, isA<Recipe>());
    },
  );

  group('fromJson', () {
    test(
      'should return a valid model when JSON data exists',
      () async {
        // arrange
        final Map<String, dynamic> jsonMap =
            json.decode(fixture('spoonacular_recipe.json'));
        // act
        final result = RecipeModel.fromJson(jsonMap);
        // assert
        expect(result, tRecipeModel);
      },
    );
  });

  group('toJson', () {
    test(
      'should return a JSON map containing the proper data',
      () async {
        // act
        final result = tRecipeModel.toJson();
        // assert
        final expectedJsonMap = {
          "id": tId,
        };
        expect(result, expectedJsonMap);
      },
    );
  });
}
