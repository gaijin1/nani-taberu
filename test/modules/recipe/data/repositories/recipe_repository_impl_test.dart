import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:nani_taberu/core/errors/exceptions.dart';
import 'package:nani_taberu/core/errors/failures.dart';
import 'package:nani_taberu/core/platform/networkinfo.dart';
import 'package:nani_taberu/modules/recipe/data/data_sourcs/recipe_data_source.dart';
import 'package:nani_taberu/modules/recipe/data/models/recipe_model.dart';
import 'package:nani_taberu/modules/recipe/data/repositories/recipe_repository_impl.dart';
import 'package:nani_taberu/modules/recipe/domain/entities/recipe.dart';

class MockRemoteDataSource extends Mock implements RecipeRemoteDataSource {}

class MockLocalDataSource extends Mock implements RecipeLocalDataSource {}

class MockNetworkInfo extends Mock implements NetworkInfo {}

void main() {
  RecipeRepositoryImpl repository;
  MockRemoteDataSource mockRemoteDataSource;
  MockLocalDataSource mockLocalDataSource;
  MockNetworkInfo mockNetworkInfo;

  setUp(() {
    mockRemoteDataSource = MockRemoteDataSource();
    mockLocalDataSource = MockLocalDataSource();
    mockNetworkInfo = MockNetworkInfo();
    repository = RecipeRepositoryImpl(
      remoteDataSource: mockRemoteDataSource,
      localDataSource: mockLocalDataSource,
      networkInfo: mockNetworkInfo,
    );
  });

  group('getRecipe (remote data source)', () {
    final tId = 1;
    final tRecipeModel = RecipeModel(id: tId);
    final Recipe tRecipe = tRecipeModel;

    test('check if device is "online"', () {
      // arrange
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      // act
      repository.getRecipe(tId);
      // assert
      verify(mockNetworkInfo.isConnected);
    });

    group('device is online', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      });
      test('returns data when remote call is successful', () async {
        // arrange
        when(mockRemoteDataSource.getRecipe(tId))
            .thenAnswer((_) async => tRecipeModel);
        // act
        final result = await repository.getRecipe(tId);
        // assert
        verify(mockRemoteDataSource.getRecipe(tId));
        expect(result, equals(Right(tRecipe)));
      });

      test('caches data when remote call is successful', () async {
        // arrange
        when(mockRemoteDataSource.getRecipe(tId))
            .thenAnswer((_) async => tRecipeModel);
        // act
        await repository.getRecipe(tId);
        // assert
        verify(mockRemoteDataSource.getRecipe(tId));
        verify(mockLocalDataSource.cacheRecipe(tRecipeModel));
      });

      test('returns ServerFailure when the remote call fails', () async {
        // arrange
        when(mockRemoteDataSource.getRecipe(tId)).thenThrow(ServerException());
        // act
        final result = await repository.getRecipe(tId);
        // assert
        verify(mockRemoteDataSource.getRecipe(tId));
        verifyZeroInteractions(mockLocalDataSource);
        expect(result, equals(Left(ServerFailure())));
      });
    });

    group('device is offline', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => false);
      });
      test('returns last locally cached data when the cached data is present',
          () async {
        // arrange
        when(mockLocalDataSource.getLastRecipe())
            .thenAnswer((_) async => tRecipeModel);
        // act
        final result = await repository.getRecipe(tId);
        // assert
        verifyZeroInteractions(mockRemoteDataSource);
        verify(mockLocalDataSource.getLastRecipe());
        expect(result, equals(Right(tRecipeModel)));
      });

      test('returns CacheFailure when there is no cached data present',
          () async {
        // arrange
        when(mockLocalDataSource.getLastRecipe()).thenThrow(CacheException());
        // act
        final result = await repository.getRecipe(tId);
        // assert
        verifyZeroInteractions(mockRemoteDataSource);
        verify(mockLocalDataSource.getLastRecipe());
        expect(result, equals(Left(CacheFailure())));
      });
    });
  });

  group('getRandomRecipe (remote data source)', () {
    final tId = 1;
    final tRecipeModel = RecipeModel(id: tId);
    final Recipe tRecipe = tRecipeModel;

    test('check if device is "online"', () {
      // arrange
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      // act
      repository.getRandomRecipe();
      // assert
      verify(mockNetworkInfo.isConnected);
    });

    group('device is online', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      });
      test('returns data when remote call is successful', () async {
        // arrange
        when(mockRemoteDataSource.getRandomRecipe())
            .thenAnswer((_) async => tRecipeModel);
        // act
        final result = await repository.getRandomRecipe();
        // assert
        verify(mockRemoteDataSource.getRandomRecipe());
        expect(result, equals(Right(tRecipe)));
      });

      test('caches data when remote call is successful', () async {
        // arrange
        when(mockRemoteDataSource.getRandomRecipe())
            .thenAnswer((_) async => tRecipeModel);
        // act
        await repository.getRandomRecipe();
        // assert
        verify(mockRemoteDataSource.getRandomRecipe());
        verify(mockLocalDataSource.cacheRecipe(tRecipeModel));
      });

      test('returns ServerFailure when the remote call fails', () async {
        // arrange
        when(mockRemoteDataSource.getRandomRecipe())
            .thenThrow(ServerException());
        // act
        final result = await repository.getRandomRecipe();
        // assert
        verify(mockRemoteDataSource.getRandomRecipe());
        verifyZeroInteractions(mockLocalDataSource);
        expect(result, equals(Left(ServerFailure())));
      });
    });

    group('device is offline', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => false);
      });
      test('returns last locally cached data when the cached data is present',
          () async {
        // arrange
        when(mockLocalDataSource.getLastRecipe())
            .thenAnswer((_) async => tRecipeModel);
        // act
        final result = await repository.getRandomRecipe();
        // assert
        verifyZeroInteractions(mockRemoteDataSource);
        verify(mockLocalDataSource.getLastRecipe());
        expect(result, equals(Right(tRecipeModel)));
      });

      test('returns CacheFailure when there is no cached data present',
          () async {
        // arrange
        when(mockLocalDataSource.getLastRecipe()).thenThrow(CacheException());
        // act
        final result = await repository.getRandomRecipe();
        // assert
        verifyZeroInteractions(mockRemoteDataSource);
        verify(mockLocalDataSource.getLastRecipe());
        expect(result, equals(Left(CacheFailure())));
      });
    });
  });
}
