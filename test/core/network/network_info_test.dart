import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:nani_taberu/core/platform/networkinfo.dart';

class MockDataConnectionChecker extends Mock implements DataConnectionChecker {}

void main() {
  NetworkInfoImpl networkInfo;
  MockDataConnectionChecker dataConnectionChecker;
  setUp(() {
    dataConnectionChecker = MockDataConnectionChecker();
    networkInfo = NetworkInfoImpl(checker: dataConnectionChecker);
  });

  group('NetworkInfoImpl', () {
    test('forwards the call to DataConnectionChecker.hasConnection', () async {
      final tHasConnectionFuture = Future.value(true);
      // arrange
      when(dataConnectionChecker.hasConnection)
          .thenAnswer((_) => tHasConnectionFuture);
      // act
      final isConnected = networkInfo.isConnected;
      // assert
      verify(dataConnectionChecker.hasConnection);
      expect(isConnected, tHasConnectionFuture);
    });
  });
}
