import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;
import 'package:nani_taberu/core/platform/networkinfo.dart';
import 'package:nani_taberu/modules/recipe/data/data_sourcs/recipe_data_source.dart';
import 'package:nani_taberu/modules/recipe/data/data_sourcs/recipe_fake_data_source.dart';
import 'package:nani_taberu/modules/recipe/data/data_sourcs/recipe_local_data_source_impl.dart';
import 'package:nani_taberu/modules/recipe/data/data_sourcs/spoonacular_data_source.dart';
import 'package:nani_taberu/modules/recipe/data/repositories/recipe_repository_impl.dart';
import 'package:nani_taberu/modules/recipe/domain/repositories/recipe_repository.dart';
import 'package:nani_taberu/modules/recipe/domain/use_cases/get_random_recipe.dart';
import 'package:nani_taberu/modules/recipe/domain/use_cases/get_recipe.dart';
import 'package:nani_taberu/modules/recipe/presentation/bloc/bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

final di = GetIt.instance;

Future<void> init() async {
  // local variables
  String spoonacularApiKey = DotEnv().env['SPOONACULAR_API_KEY'];

  //! Features - Recipe

  //Bloc
  di.registerFactory(
    () => RecipeSearchBloc(
      concrete: di(),
      random: di(),
    ),
  );

  // Use cases
  di.registerLazySingleton(() => GetRandomRecipe(di()));
  di.registerLazySingleton(() => GetRecipe(di()));

  // Repository
  di.registerLazySingleton<RecipeRepository>(
    () => RecipeRepositoryImpl(
      remoteDataSource: di(),
      localDataSource: di(),
      networkInfo: di(),
      fakeDataSource: di(),
    ),
  );

  // Data sources
  di.registerLazySingleton<RecipeRemoteDataSource>(
    () => SpoonacularDataSource(
      client: di(),
      apiKey: spoonacularApiKey,
    ),
  );

  di.registerLazySingleton<RecipeLocalDataSource>(
    () => RecipeLocalDataSourceImpl(preferences: di()),
  );

  di.registerLazySingleton<RecipeFakeDataSource>(
    () => RecipeFakeDataSource(),
  );

  //! Core
  di.registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl(checker: di()));

  //! External
  final sharedPreferences = await SharedPreferences.getInstance();
  di.registerLazySingleton(() => sharedPreferences);
  di.registerLazySingleton(() => http.Client());
  di.registerLazySingleton(() => DataConnectionChecker());
}
