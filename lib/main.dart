import 'package:flutter/material.dart';
import 'package:custom_splash/custom_splash.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:nani_taberu/app_theme.dart';
import 'package:nani_taberu/di_container.dart' as diContainer;
import 'package:nani_taberu/modules/recipe/presentation/pages/random_recipe_picker.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await DotEnv().load('.env');
  await diContainer.init();
  return runApp(MaterialApp(
    theme: appTheme,
    home: RandomRecipePicker(),
  ));
}

class AppWithSplan extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: appTheme,
      home: CustomSplash(
        imagePath: 'assets/images/sushi-splash-1.jpg',
        backGroundColor: Colors.black,
        animationEffect: 'zoom-in',
        logoSize: 200,
        home: RandomRecipePicker(),
        duration: 2500,
        type: CustomSplashType.StaticDuration,
      ),
    );
  }
}
