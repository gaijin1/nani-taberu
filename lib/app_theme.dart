import 'package:flutter/material.dart';

class NaniTaberuAppThemeParams {
  NaniTaberuAppThemeParams._();
  static const Color darkIndigo = Color(0xFF1A237E);
}

ThemeData appTheme = ThemeData(
    primaryColor: NaniTaberuAppThemeParams.darkIndigo,
    fontFamily: 'RobotoSlab');
