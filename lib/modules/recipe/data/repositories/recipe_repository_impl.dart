import 'package:dartz/dartz.dart';
import 'package:flutter/foundation.dart';
import 'package:meta/meta.dart';
import 'package:nani_taberu/core/errors/exceptions.dart';
import 'package:nani_taberu/core/errors/failures.dart';
import 'package:nani_taberu/core/platform/networkinfo.dart';
import 'package:nani_taberu/modules/recipe/data/data_sourcs/recipe_data_source.dart';
import 'package:nani_taberu/modules/recipe/data/data_sourcs/recipe_fake_data_source.dart';
import 'package:nani_taberu/modules/recipe/domain/entities/recipe.dart';
import 'package:nani_taberu/modules/recipe/domain/repositories/recipe_repository.dart';

typedef Future<Recipe> _ConcreteOrRandomChooser();

class RecipeRepositoryImpl implements RecipeRepository {
  final RecipeRemoteDataSource remoteDataSource;
  final RecipeLocalDataSource localDataSource;
  final RecipeFakeDataSource fakeDataSource;
  final NetworkInfo networkInfo;

  RecipeRepositoryImpl({
    @required this.remoteDataSource,
    @required this.localDataSource,
    @required this.networkInfo,
    this.fakeDataSource,
  });

  @override
  Future<Either<Failure, Recipe>> getRandomRecipe() async =>
      _getRandomOrSpecificRecipe(() => kReleaseMode
          ? remoteDataSource.getRandomRecipe()
          : fakeDataSource.getRecipe(1));

  @override
  Future<Either<Failure, Recipe>> getRecipe(int id) async =>
      _getRandomOrSpecificRecipe(() => remoteDataSource.getRecipe(id));

  Future<Either<Failure, Recipe>> _getRandomOrSpecificRecipe(
      _ConcreteOrRandomChooser chooser) async {
    bool isConnected = await networkInfo.isConnected;
    if (!isConnected) {
      try {
        final recipeModel = await localDataSource.getLastRecipe();
        return Right(recipeModel);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
    try {
      final recipeModel = await chooser();
      localDataSource.cacheRecipe(recipeModel);
      return Right(recipeModel);
    } on ServerException {
      return Left(ServerFailure());
    }
  }
}
