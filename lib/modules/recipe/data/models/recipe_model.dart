import 'package:meta/meta.dart';
import 'package:nani_taberu/modules/recipe/domain/entities/recipe.dart';

class RecipeModel extends Recipe {
  final int id;
  final String title;
  final String sourceName;
  final String imageUrl;
  final String instructions;
  final int readyInMinutes;
  final int servings;

  RecipeModel({
    @required this.id,
    this.title,
    this.sourceName,
    this.imageUrl,
    this.instructions,
    this.readyInMinutes,
    this.servings,
  }) : super(
          id: id,
          title: title,
          sourceName: sourceName,
          imageUrl: imageUrl,
          instructions: instructions,
          readyInMinutes: readyInMinutes,
          servings: servings,
        );

  factory RecipeModel.fromJson(Map<String, dynamic> jsonMap) => RecipeModel(
        id: jsonMap['id'],
        title: jsonMap['title'],
        sourceName: jsonMap['sourceName'],
        imageUrl: jsonMap['image'],
        instructions: jsonMap['instructions'],
        readyInMinutes: jsonMap['readyInMinutes'],
        servings: jsonMap['servings'],
      );

  Map<String, dynamic> toJson() {
    return {
      'id': id,
    };
  }
}
