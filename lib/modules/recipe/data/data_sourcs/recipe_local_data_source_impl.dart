import 'dart:convert';
import 'package:nani_taberu/core/errors/exceptions.dart';
import 'package:nani_taberu/modules/recipe/data/data_sourcs/recipe_data_source.dart';
import 'package:nani_taberu/modules/recipe/data/models/recipe_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:meta/meta.dart';

String CACHED_RECIPE_KEY = 'CACHED_RECIPE_KEY';

class RecipeLocalDataSourceImpl implements RecipeLocalDataSource {
  SharedPreferences preferences;

  RecipeLocalDataSourceImpl({@required this.preferences});

  @override
  Future<void> cacheRecipe(RecipeModel recipeToCache) {
    preferences.setString(
        CACHED_RECIPE_KEY, json.encode(recipeToCache.toJson()));
    return null;
  }

  @override
  Future<RecipeModel> getLastRecipe() {
    String jsonData = preferences.getString(CACHED_RECIPE_KEY);
    if (jsonData == "" || jsonData == null) {
      throw NoLocalDataException();
    }
    return Future.value(RecipeModel.fromJson(json.decode(jsonData)));
  }
}
