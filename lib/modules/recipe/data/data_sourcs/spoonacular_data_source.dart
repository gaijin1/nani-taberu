import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:nani_taberu/core/errors/exceptions.dart';
import 'package:nani_taberu/modules/recipe/data/data_sourcs/recipe_data_source.dart';
import 'package:nani_taberu/modules/recipe/data/models/recipe_model.dart';
import 'package:meta/meta.dart';

String SPOONACULAR_API_URL = 'https://api.spoonacular.com';
String SPOONACULAR_API_RANDOM_RECIPE_URL =
    SPOONACULAR_API_URL + '/recipes/random';

typedef Future<http.Response> CALLBACK();

class SpoonacularDataSource implements RecipeRemoteDataSource {
  http.Client client;
  String apiKey;

  SpoonacularDataSource({
    @required this.client,
    @required this.apiKey,
  });

  @override
  Future<RecipeModel> getRandomRecipe() async => _getRecipeWithCallback(() =>
      client.get(_buildEndpoint(SPOONACULAR_API_RANDOM_RECIPE_URL),
          headers: _buildHeaders()));

  @override
  Future<RecipeModel> getRecipe(int number) async =>
      _getRecipeWithCallback(() => client.get(
          _buildEndpoint(SPOONACULAR_API_URL + '/$number/information'),
          headers: _buildHeaders()));

  Future<RecipeModel> _getRecipeWithCallback(CALLBACK cb) async {
    final response = await cb();
    if (response.statusCode == 200) {
      final recipeModel = RecipeModel.fromJson(json.decode(response.body));
      return recipeModel;
    }

    // if (response.statusCode >= 400) {
    //   throw ServerException();
    // }

    // let's throw anyway;
    throw ServerException();
  }

  String _buildEndpoint(String url) => '$url?apiKey=$apiKey';

  Map<String, String> _buildHeaders() {
    return {
      'Content-Type': 'application/json',
    };
  }
}
