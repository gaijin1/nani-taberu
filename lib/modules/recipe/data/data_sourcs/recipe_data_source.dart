import 'package:nani_taberu/modules/recipe/data/models/recipe_model.dart';

abstract class RecipeRemoteDataSource {
  Future<RecipeModel> getRecipe(int number);
  Future<RecipeModel> getRandomRecipe();
}

abstract class RecipeLocalDataSource {
  /// Gets the cached [RecipeModel] which was gotten the last time
  /// the user had an internet connection.
  ///
  /// Throws [NoLocalDataException] if no cached data is present.
  Future<RecipeModel> getLastRecipe();

  Future<void> cacheRecipe(RecipeModel recipeToCache);
}
