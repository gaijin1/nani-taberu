import 'package:nani_taberu/modules/recipe/data/data_sourcs/recipe_data_source.dart';
import 'package:nani_taberu/modules/recipe/data/models/recipe_model.dart';

class RecipeFakeDataSource implements RecipeRemoteDataSource {
  @override
  Future<RecipeModel> getRandomRecipe() => getRecipe(0);

  @override
  Future<RecipeModel> getRecipe(int number) async => RecipeModel(
        id: 222227,
        title: "Tortilla tapas",
        sourceName: "Source of the recipe",
        imageUrl: "https://spoonacular.com/recipeImages/222227-556x370.jpg",
        instructions:
            "Preheat oven to fan 180C/ conventional 200C/gas 6. Heat 2 tbsp of olive oil in a pan. Tip in onion and potatoes and fry gently for 20 minutes until the potatoes are tender, turning frequently, adding garlic and peppers for the last 5 minutes. Tip into a large bowl, season and cool for 5 minutes.\nStir in the eggs, chilli flakes and parsley. Leave to sit for 5 minutes, then put a nonstick 20cm square tin in the oven to heat up. After 5 minutes, remove tin and brush with oil. pour in the egg mixture and return to oven.\nCook for 15-20 minutes. (Check its ready by pressing the top lightly. If it is still runny, return to the oven for a couple of minutes.) Once cooked, remove and leave to cool for 5 minutes, then turn out on to a board. Cut into squares and serve.",
        readyInMinutes: 45,
        servings: 2,
      );
}
