import 'package:flutter/material.dart';

class RandomRecipePickerPageContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Center(
          child: Text(
            "Want to know",
            textDirection: TextDirection.ltr,
            style: TextStyle(
              fontSize: 32,
              color: Colors.black87,
            ),
          ),
        ),
        Center(
          child: Text(
            "Today's menu?",
            textDirection: TextDirection.ltr,
            style: TextStyle(
              fontSize: 24,
              color: Colors.black87,
            ),
          ),
        ),
      ],
    );
  }
}
