import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:nani_taberu/modules/recipe/domain/entities/recipe.dart';

class RecipePageFull extends StatelessWidget {
  final Recipe recipe;

  RecipePageFull({@required this.recipe});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(recipe.title),
      ),
      body: Column(
        children: <Widget>[
          Image.network(
            recipe.imageUrl,
            fit: BoxFit.fill,
          ),
          SizedBox(height: 10),
          Center(
            child: RaisedButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text('Go back!'),
            ),
          ),
        ],
      ),
    );
  }
}
