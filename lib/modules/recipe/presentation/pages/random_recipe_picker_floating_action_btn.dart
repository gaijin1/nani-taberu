import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nani_taberu/modules/recipe/presentation/bloc/recipe_search_bloc.dart';
import 'package:nani_taberu/modules/recipe/presentation/bloc/recipe_search_event.dart';

class RandomRecipePickerFloatingActionBtn extends StatefulWidget {
  @override
  _RandomRecipePickerFloatingActionBtnState createState() =>
      _RandomRecipePickerFloatingActionBtnState();
}

class _RandomRecipePickerFloatingActionBtnState
    extends State<RandomRecipePickerFloatingActionBtn>
    with SingleTickerProviderStateMixin {
  AnimationController _searchController;

  @override
  void initState() {
    super.initState();
    _searchController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 250));
  }

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      child: AnimatedIcon(
        icon: AnimatedIcons.search_ellipsis,
        progress: this._searchController,
      ),
      onPressed: () {
        setState(() {
          _searchController.forward();
        });
        Timer(Duration(seconds: 3), () {
          setState(() {
            _searchController.reverse();
          });
        });
        BlocProvider.of<RecipeSearchBloc>(context).add(GetRandomRecipeEvent());
      },
    );
  }
}
