import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nani_taberu/app_theme.dart';
import 'package:nani_taberu/di_container.dart';
import 'package:nani_taberu/modules/recipe/presentation/bloc/bloc.dart';
import 'package:nani_taberu/modules/recipe/presentation/pages/random_recipe_picker_floating_action_btn.dart';
import 'package:nani_taberu/modules/recipe/presentation/pages/recipe_page.dart';
import 'package:nani_taberu/modules/recipe/presentation/pages/random_recipe_picker_page_content.dart';
import 'package:nani_taberu/modules/recipe/presentation/widgets/error_message_display.dart';
import 'package:nani_taberu/modules/recipe/presentation/widgets/loading_widget.dart';

class RandomRecipePicker extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => di<RecipeSearchBloc>(),
      child: _buildScaffold(context),
    );
  }

  Widget _buildScaffold(BuildContext context) {
    return Scaffold(
      body: _buildScaffoldBody(context),
      bottomNavigationBar: BottomAppBar(
        color: NaniTaberuAppThemeParams.darkIndigo,
        shape: CircularNotchedRectangle(),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(25),
            )
          ],
        ),
      ),
      floatingActionButton: RandomRecipePickerFloatingActionBtn(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }

  Widget _buildScaffoldBody(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: Colors.white),
      child: Column(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: BlocBuilder<RecipeSearchBloc, RecipeSearchState>(
              builder: (context, state) {
                if (state is InitialRecipeSearchState) {
                  return RandomRecipePickerPageContent();
                } else if (state is LoadingState) {
                  return LoadingWidget();
                } else if (state is LoadedState) {
                  return RecipePage(recipe: state.recipe);
                } else if (state is ErrorState) {
                  return ErrorMessageDisplay(message: state.message);
                } else {
                  return ErrorMessageDisplay(
                      message: 'Something went wrong \n 😖');
                }
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 32),
            child: Text('Find it now!', style: TextStyle(fontSize: 24)),
          )
        ],
      ),
    );
  }
}
