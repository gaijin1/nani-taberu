import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:nani_taberu/modules/recipe/domain/entities/recipe.dart';
import 'package:nani_taberu/modules/recipe/presentation/pages/recipe_page_full.dart';

class RecipePage extends StatelessWidget {
  final Recipe recipe;

  RecipePage({@required this.recipe});

  @override
  Widget build(BuildContext context) {
    Widget imageSection = GestureDetector(
      child: FadeInImage.assetNetwork(
        placeholder: "assets/images/placeholder.gif",
        image: recipe.imageUrl,
        fit: BoxFit.fitWidth,
      ),
      onTap: () => _navigateToFullRecipePage(context),
    );

    Color iconColor = Theme.of(context).primaryColor;
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                InkWell(
                  onTap: () => _navigateToFullRecipePage(context),
                  child: Container(
                    padding: const EdgeInsets.only(bottom: 8),
                    child: Text(
                      recipe.title,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 24,
                      ),
                    ),
                  ),
                ),
                Text(
                  recipe.sourceName,
                  style: TextStyle(
                    color: Colors.grey[500],
                  ),
                ),
              ],
            ),
          ),
          InkWell(
            onTap: () {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: Text("Add to favorite"),
                    content: Text("feature coming soon"),
                    actions: <Widget>[
                      FlatButton(
                        child: new Text("Close"),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  );
                },
              );
            },
            child: Icon(
              Icons.bookmark_border,
              color: Colors.red[500],
            ),
          ),
        ],
      ),
    );

    Widget buttonSection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildButtonColumn(
              iconColor, Icons.timelapse, "${recipe.readyInMinutes} MINS"),
          _buildButtonColumn(
              iconColor,
              MaterialCommunityIcons.silverware_fork_knife,
              "${recipe.servings} PERSONS"),
          _buildButtonColumn(
              iconColor, MaterialCommunityIcons.heart_multiple, '45 LIKES'),
        ],
      ),
    );

    return Scaffold(
      appBar: AppBar(
        title: Text(recipe.title),
      ),
      body: Column(
        children: <Widget>[
          imageSection,
          // SizedBox(height: 10),
          titleSection,
          buttonSection,
        ],
      ),
    );
  }

  void _navigateToFullRecipePage(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => RecipePageFull(recipe: recipe),
      ),
    );
  }

  Column _buildButtonColumn(Color color, IconData icon, String label) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(icon, color: color),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            label,
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: color,
            ),
          ),
        ),
      ],
    );
  }
}
