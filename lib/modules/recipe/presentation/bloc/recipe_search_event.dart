import 'package:equatable/equatable.dart';

abstract class RecipeSearchEvent extends Equatable {
  const RecipeSearchEvent();
}

class InitializeEvent extends RecipeSearchEvent {
  @override
  List<Object> get props => null;
}

class GetRandomRecipeEvent extends RecipeSearchEvent {
  @override
  List<Object> get props => null;
}

class GetRecipeEvent extends RecipeSearchEvent {
  final int id;

  GetRecipeEvent(this.id);

  @override
  List<Object> get props => [id];
}
