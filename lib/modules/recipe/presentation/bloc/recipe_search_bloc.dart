import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:nani_taberu/core/errors/failures.dart';
import 'package:nani_taberu/modules/recipe/domain/use_cases/base_use_case.dart';
import 'package:nani_taberu/modules/recipe/domain/use_cases/get_random_recipe.dart';
import 'package:nani_taberu/modules/recipe/domain/use_cases/get_recipe.dart';
import './bloc.dart';

const String SERVER_FAILURE_MESSAGE = 'Server Failure';
const String CACHE_FAILURE_MESSAGE = 'Cache Failure';
const String DEFAULT_FAILURE_MESSAGE = 'Unkown Failure';

class RecipeSearchBloc extends Bloc<RecipeSearchEvent, RecipeSearchState> {
  GetRandomRecipe random;
  GetRecipe concrete;

  RecipeSearchBloc({
    @required this.random,
    @required this.concrete,
  });

  @override
  RecipeSearchState get initialState => InitialRecipeSearchState();

  @override
  Stream<RecipeSearchState> mapEventToState(
    RecipeSearchEvent event,
  ) async* {
    yield InitialRecipeSearchState();
    yield LoadingState();
    if (event is InitializeEvent) {
      yield InitialRecipeSearchState();
    }
    if (event is GetRandomRecipeEvent) {
      final failureOrRecipe = await random(NoParams());
      yield failureOrRecipe.fold(
        _mapErrorState,
        (recipe) => LoadedState(recipe: recipe),
      );
    }
  }

  RecipeSearchState _mapErrorState(Failure failure) =>
      ErrorState(message: _getFailureMessage(failure));

  String _getFailureMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return SERVER_FAILURE_MESSAGE;
      case CacheFailure:
        return CACHE_FAILURE_MESSAGE;
      default:
        return DEFAULT_FAILURE_MESSAGE;
    }
  }
}
