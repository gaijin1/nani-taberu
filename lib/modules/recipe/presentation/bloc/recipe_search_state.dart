import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:nani_taberu/modules/recipe/domain/entities/recipe.dart';

abstract class RecipeSearchState extends Equatable {
  const RecipeSearchState();
}

class InitialRecipeSearchState extends RecipeSearchState {
  @override
  List<Object> get props => [];
}

class EmptyState extends RecipeSearchState {
  @override
  List<Object> get props => null;
}

class LoadingState extends RecipeSearchState {
  @override
  List<Object> get props => null;
}

class LoadedState extends RecipeSearchState {
  final Recipe recipe;

  LoadedState({@required this.recipe});

  @override
  List<Object> get props => [recipe.id];
}

class ErrorState extends RecipeSearchState {
  final String message;

  ErrorState({this.message});

  @override
  List<Object> get props => [message];
}
