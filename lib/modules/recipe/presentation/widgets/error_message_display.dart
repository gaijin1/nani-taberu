import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:nani_taberu/modules/recipe/presentation/bloc/bloc.dart';

class ErrorMessageDisplay extends StatelessWidget {
  final String message;

  const ErrorMessageDisplay({Key key, @required this.message})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Timer(Duration(seconds: 3), () {
      BlocProvider.of<RecipeSearchBloc>(context).add(InitializeEvent());
    });
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Center(
            child: Text(
              message,
              textDirection: TextDirection.ltr,
              style: TextStyle(
                fontSize: 32,
                color: Colors.black87,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
