import 'package:dartz/dartz.dart';
import 'package:nani_taberu/core/errors/failures.dart';
import 'package:nani_taberu/modules/recipe/domain/entities/recipe.dart';

abstract class RecipeRepository {
  Future<Either<Failure, Recipe>> getRecipe(int id);
  Future<Either<Failure, Recipe>> getRandomRecipe();
}
