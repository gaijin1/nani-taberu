import 'package:dartz/dartz.dart';
import 'package:nani_taberu/core/errors/failures.dart';
import 'package:nani_taberu/modules/recipe/domain/entities/recipe.dart';
import 'package:nani_taberu/modules/recipe/domain/repositories/recipe_repository.dart';
import 'package:nani_taberu/modules/recipe/domain/use_cases/base_use_case.dart';

class GetRandomRecipe extends UseCase<Recipe, NoParams> {
  final RecipeRepository repository;

  GetRandomRecipe(this.repository);

  /// callable
  ///
  /// Accepts [NoParams] parameter as argument and
  /// returns a [Future<Either<Failure, Recipe>>].
  @override
  Future<Either<Failure, Recipe>> call(NoParams params) async =>
      repository.getRandomRecipe();
}
