import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:nani_taberu/core/errors/failures.dart';
import 'package:nani_taberu/modules/recipe/domain/entities/recipe.dart';
import 'package:nani_taberu/modules/recipe/domain/repositories/recipe_repository.dart';
import 'package:nani_taberu/modules/recipe/domain/use_cases/base_use_case.dart';

class GetRecipe extends UseCase<Recipe, Params> {
  final RecipeRepository repository;

  GetRecipe(this.repository);

  @override
  Future<Either<Failure, Recipe>> call(Params params) async =>
      repository.getRecipe(params.id);
}

class Params extends Equatable {
  final int id;

  Params({@required this.id}) : super();

  @override
  List<Object> get props => [id];
}
