// TO-DO: Add some more fields to the Recipe entity

import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class Recipe extends Equatable {
  final int id;
  final String title;
  final String sourceName;
  final String imageUrl;
  final String instructions;
  final int readyInMinutes;
  final int servings;

  Recipe({
    @required this.id,
    this.title,
    this.sourceName,
    this.imageUrl,
    this.instructions,
    this.readyInMinutes,
    this.servings,
  });

  @override
  List<Object> get props => [id];
}
